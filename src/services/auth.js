const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const crypto = require('crypto')
const { validationResult } = require('express-validator')

const database = require("../utils/database")
const { authLimiter, loginSchema, registerSchema } = require("./auth-schema")

const TOKEN_SECRET = process.env.TOKEN_SECRET
if (TOKEN_SECRET === undefined) {
  throw new Error("Invalid TOKEN_SECRET!") 
}

const expiresInSeconds = 60 * 60 * 24 * 7 // 7 days

/* 
  token content template: {
    "user_id",
    "name",
    "email"
  }
*/

/* We store salted (32 byte salt) SHA256 password hashes */

function validateSchema(req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({
      success: false,
      message: errors.array({onlyFirstError: true})[0].msg
    })
  } else {
    next()
  }
}

const route = express.Router()

route.use(bodyParser.json())

/* Returns session information and whether you're authenticated to the server */
route.post("/", 
  session,
  (req, res) => {
    const session = req.session
    if (session) {
      return res.status(200).json({
        authenticated: true,
        session,
      })
    } else {
      res.status(200).json({
        authenticated: false
      })
    }
  }
)

/* Log in using email and password */
route.post("/login",
  authLimiter,
  loginSchema,
  validateSchema,
  async (req, res) => {
    const { email, password } = req.body

    if (!(email && password)) {
      return res.status(400).json({ error: "Missing email or password" })
    }

    var user
    try {
      user = await verifyCredentials(email, password)
    } catch (e) {
      return res.status(400).json({
        success: false,
        message: e.message
      })
    }
    console.log(user)

    const token = createToken(user)

    res.cookie("token", token, {
      maxAge: expiresInSeconds * 1000,
      sameSite: "strict",
      httpOnly: true,
    })

    return res.status(200).json({
      success: true,
      token: token,
      message: "Successfully logged in!"
    })
  }
);

/* Register a new user with name, email, and password */
route.post("/register", 
  authLimiter,
  registerSchema,
  validateSchema,
  async (req, res) => {
    const { name, email, password } = req.body
    if (!(name && email && password)) {
      return res.status(400).json({
        success: false,
        message: "Missing name, email or password!"
      })
    }
    
    var user
    try {
      user  = await createUser(name, email, password)
    } catch (e) {
      return res.status(400).json({
        success: false,
        message: e.message,
      })
    }

    const token = createToken(user)
    res.cookie("token", token, {
      maxAge: expiresInSeconds * 1000,
      sameSite: "strict",
      httpOnly: true,
    })

    return res.status(200).json({
      success: true,
      token: token,
      message: "Registered user successfully",
    })
  }
)

route.post("/logout", 
  authLimiter,
  (req, res) => {
    res.clearCookie("token")
    res.status(200).json({
      success: true, 
      message: "Logout successfully"
    })
  }
)

function session(req, res, next) {
  /* Checks cookies for valid session token.
     If it exists, it will be decoded and stored at req.session
     If there is no req.session, the user is not logged in. 
  */
  const cookie_token = req.cookies.token
  if (cookie_token) {
    var session
    try {
      session = verifyToken(cookie_token)
    } catch (e) {
      console.error(e)
      return next()
    }
    req.session = session
  }
  return next()
}

function require_auth(req, res, next) {
  /* Checks to make sure the user has a valid session token. */
  if (req.session && 
      req.session.user_id &&
      req.session.email &&
      req.session.name) {
    return next()
  } else {
    return res.redirect("/login")
  }
}

async function verifyCredentials(email, password) {
  /* If successful, returns user object:
    {
      user_id,
      name,
      email
    }
    Otherwise, errors.
  */
  var conn
  try {
    conn = await database.createConnection()
  } catch (e) {
    console.error(e)
    throw new Error("Could not connect to database")
  }
  const [rows] = await conn.query("SELECT * FROM users WHERE email = ?", 
    [email]) 
  
  await conn.release()
  
  if (rows.length == 0) {
    throw new Error("Couldn't find a user with that email!")
  }

  const user = rows[0]

  const {password_hash, salt} = user
  const input = password + salt
  const hash = crypto.createHash("sha256")
  hash.update(input)
  const hashed = hash.digest("hex")

  if (hashed == password_hash) {
    return {
      email,
      name: user.name,
      user_id: user.user_id
    }
  } else {
    throw new Error("Incorrect password")
  }
}

async function createUser(name, email, password) {
  /* Returns user object on success. Errors on failure */
  var conn 
  try {
    conn = await database.createConnection()
  } catch (e) {
    console.error(e)
    throw new Error("Could not connect to database")
  }
  var [rows] = await conn.query("SELECT user_id FROM users WHERE email = ?",
    [email])
  if (rows.length > 0) {
    await conn.release()
    throw new Error(`User ${email} already exists`)
  }

  const salt = crypto.randomBytes(32).toString("base64")
  const hash = crypto.createHash("sha256")
  const input = password + salt
  hash.update(input)
  const hashed = hash.digest("hex")

  try {
    var [result] = await conn.query("INSERT INTO users (name, email, salt, password_hash) VALUES (?, ?, ?, ?)",
      [name, email, salt, hashed])
    const user = {
      user_id: result.insertId,
      name,
      email,
    }
    console.log(user)
    return user
  } catch (e) {
    await conn.release()
    throw new Error("Something went wrong: " + e.message)
  }
}

function createToken({user_id, name, email}) {
  /*  Creates a token 
      Errors if invalid inputs
  */
  if (!(user_id && name && email)) {
    throw new Error("user_id, name and email are required")
  }

  return jwt.sign({
    user_id: user_id,
    name: name,
    email: email
  }, TOKEN_SECRET, {
    expiresIn: expiresInSeconds,
  })
}

function verifyToken(token) {
  /* returns token contents if valid token
     otherwise errors.
  */
  var {user_id, email, name} = jwt.verify(token, TOKEN_SECRET)
  return {user_id, email, name}
}

module.exports = {
  route: route,
  session: session,
  require_auth: require_auth,
}
