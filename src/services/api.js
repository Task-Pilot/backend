const express = require('express')
const bodyParser = require('body-parser')

const database = require('../utils/database')
const auth = require('./auth')
const { parseDate, deparseDate } = require("../utils/format-date")

const api = express.Router();

// Require authentication to use the API
api.use(bodyParser.json())
api.use(auth.require_auth)

api.use((req, res, next) => {
	console.log(req.url, req.body)
	next()
})

api.post("/getTasks", async (req, res) => {
	var { page, statusFilter, categoryFilter, sort_by, limit} = req.body

	// Page and limit validation
	page = parseInt(page) || 0
	limit = parseInt(limit) || 20
	if (page < 0 || limit < 1) {
		return res.status(400).json({
			success: false,
			message: "Invalid page number or limit specified",
		})
	}

	// Sort by validation
	const sortOptions = {
		soonest: "ORDER BY date, task_id", 
		latest: "ORDER BY date DESC, task_id"
	}
	if (sort_by && sortOptions[sort_by] === undefined) {
		return res.status(400).json({
      success: false,
			message: `Invalid sort_by specified: ${sort_by}`,
		})
	}
	const sortByQuery = sortOptions[sort_by] || ""

	var statusFilterQuery = ""
	if (statusFilter === "any") {
		statusFilterQuery = ""
	} else if (statusFilter === "hide-completed") {
		statusFilterQuery = "AND status != 2"
	} else if (statusFilter === "only-completed") {
		statusFilterQuery = "AND status = 2"
	}

	var categoryFilterQuery = ""
  if (categoryFilter === "any") {
		categoryFilterQuery = ""
	} else {
		categoryFilterQuery = "AND category_id = ?"
	}

	var conn
	try {
		conn = await database.createConnection();
	} catch (e) {
		console.error(e)
		return res.status(500).json({
      success: false,
			message: "Could not connect to the database"
		})
	}
	const [rows] = await conn.query(`SELECT text, date, status, category_id, task_id 
		FROM tasks 
		WHERE owner = ? 
		${statusFilterQuery} 
		${categoryFilterQuery} 
		${sortByQuery} 
		LIMIT ? OFFSET ?`, 
		[req.session.user_id].concat(categoryFilter !== "any" ? [categoryFilter] : []).concat([limit, page * limit]));
	
	// Before we send the rows, convert them to UTC.


	rows.forEach(row => {
		row.date = parseDate(row.date)
	})

	conn.release();

	return res.status(200).json({
		success: true,
		tasks: rows
	});
});

api.post("/getCategories", async (req, res) => {
	var conn
	try {
		conn = await database.createConnection();
	} catch (e) {
		console.error(e)
		return res.status(500).json({
      success: false,
			message: "Could not connect to the database"
		})
	}

	const [rows] = await conn.query("SELECT name, category_id, active FROM categories WHERE owner = ? ORDER BY active DESC",
		[req.session.user_id])

	conn.release();
	
	return res.status(200).json({
		success: true,
		categories: rows
	})
})

api.post("/addCategory", async (req, res) => {
	const name = req.body.name

	var conn
	try {
		conn = await database.createConnection();
	} catch (e) {
		console.error(e)
		return res.status(500).json({
      success: false,
			message: "Could not connect to the database"
		})
	}

	const [result] = await conn.query("INSERT INTO categories(owner, name) VALUES(?, ?)",
		[req.session.user_id, name])

	const category_id = result.insertId

	conn.release();

	return res.status(200).json({
    success: true,
		category_id
	})

})

api.put("/updateCategory", async (req, res) => {
	const category = req.body.category
	const {category_id, name, active} = category
	const owner = req.session.user_id

	var conn
	try {
		conn = await database.createConnection();
	} catch (e) {
		console.error(e)
		return res.status(500).json({
      success: false,
			message: "Could not connect to the database"
		})
	}

	try {
		const [result] = await conn.query(`UPDATE categories
			SET active = ?,
			name = ?
			WHERE category_id = ? AND owner = ?`,
			[active, name, category_id, owner])

		if (result.affectedRows > 0) {
			conn.release()
			return res.status(200).json({
				success: true,
				message: "Updated category"
			})
		} else {
			conn.release()
			return res.status(400).json({
				success: false,
				message: "Could not update category!"
			})
		}
	} catch (e) {
		conn.release()
		console.log(e)
		return res.status(500).json({
			success: false,
			message: "Could not update category!"
		})
	}
})

api.delete("/deleteCategory", async (req, res) => {
	const category_id = req.body.category_id
	const user_id = req.session.user_id

	var conn
	try {
		conn = await database.createConnection();
	} catch (e) {
		console.error(e)
		return res.status(500).json({
      success: false,
			message: "Could not connect to the database"
		})
	}

	try {
		const [result] = await conn.query("DELETE FROM categories WHERE category_id = ? AND owner = ?",
			[category_id, user_id])
		
		if (result.affectedRows > 0) {
			conn.release();
			return res.status(200).json({
				success: true,
				message: "Successfully deleted category",
			})
		} else {
			conn.release();
			return res.status(400).json({
				success: false,
				message: "Category does not exist",
			})
		}
	} catch (e) { 
		console.error(e)
		conn.release()
		return res.status(400).json({
			success: false,
			message: "Something went wrong!",
		})
	}
})

api.post("/addTask", async (req, res) => {
	if (!(req.body.task !== undefined 
		&& req.body.task.date !== undefined
		&& req.body.task.status !== undefined
		&& req.body.task.text !== undefined
		&& req.body.task.category_id !== undefined)) {
		return res.status(400).json({
      success: false,
			message: "Invalid task data",
			task: req.body.task,
		})
	}

	var conn
	try {
		conn = await database.createConnection();
	} catch (e) {
		return res.status(500).json({
      success: false,
			message: "Could not connect to the database"
		})
	}
	const user_id = req.session.user_id
	const task = req.body.task;

	if (task.category_id === "None") {
		task.category_id = null
	}

	console.log("Creating task:", task)
	try {
		const [result] = await conn.query(`INSERT INTO 
			tasks(text, date, status, category_id, owner) 
			VALUES(?, ?, ?, ?, ?)`,
			[task.text, task.date, task.status, task.category_id, user_id]);
		const task_id = result.insertId
		res.status(200).json({
			success: true,
			task_id,
			message: "Successfully added task!"
		})
	} catch (e) {
		res.status(500).json({
			success: false,
			message: "Failed to add task!",
			error: e.toString() 
		})
	}
	conn.release()
});

api.put("/updateTask", async (req, res) => {
	const task = req.body.task
	const user_id = req.session.user_id

	if (!(task.text !== undefined && 
				task.status !== undefined && 
				task.date !== undefined && 
				task.task_id !== undefined && 
				task.category_id !== undefined)) {
		return res.status(400).json({
			success: false,
			message: "Invalid task",
		})
	}

	var conn
	try {
		conn = await database.createConnection();
	} catch (e) {
		console.error(e)
		return res.status(500).json({
      success: false,
			message: "Could not connect to the database"
		})
	}

	if (task.category_id === "None") {
		task.category_id = null
	}

	try {
		const [result] = await conn.query(`UPDATE tasks 
			SET text = ?,
			status = ?,
			date = ?,
			category_id = ?
			WHERE task_id = ?
			AND owner = ?`,
			[task.text, task.status, task.date, task.category_id, task.task_id, user_id]);
		conn.release();
		if (result.affectedRows > 0) {
			return res.status(200).json({
				success: true,
				message: "Task was successfully updated"
			})
		}
	} catch (e) {
		console.log("Error updating task:", task, e)
		conn.release()
		return res.status(500).json({
			success: false,
			message: "Failed to update!",
			error: e
		})
	}
});

api.delete("/deleteTask", async (req, res) => {
	const user_id = req.session.user_id
	var task_id = req.body.task_id
	task_id = parseInt(task_id)
	if (!task_id) {
		return res.status(400).json({
      success: false,
			message: "Invalid task_id",
		})
	}

	var conn
	try {
		conn = await database.createConnection();
	} catch (e) {
		return res.status(500).json({
      success: false,
			message: "Could not connect to the database"
		})
	}

	try {
		const [result] = await conn.query(`DELETE FROM tasks WHERE task_id = ? AND owner = ?`, 
			[task_id, user_id]);

		if (result.affectedRows > 0) {
			res.status(200).json({
				success: true,
				message: "Successfully deleted task!"
			});
		} else {
			res.status(400).json({
				success: false,
				message: "Task did not exist, cannot delete."
			})
		}
	} catch (e) {
		console.error(e)
		res.status(500).json({
      success: false,
			message: "Failed to delete!"
		})
	}
	conn.release();
});

module.exports = api