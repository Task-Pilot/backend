const { checkSchema } = require('express-validator')
const rateLimit = require("express-rate-limit")

const authLimiter = rateLimit({
  windowMs: 6 * 60 * 60 * 1000, // 1 hr window
  max: 20,
  message: {
    success: false,
    messsage: "You exceeded 20 authentication requests in 6 hours."
  },
  standardHeaders: true,
})

const loginSchema = checkSchema({
  email: {
    isEmail: true,
    notEmpty: true,
    trim: true,
    normalizeEmail: true,
    errorMessage: "Please enter a valid email address"
  },
  password: {
    notEmpty: true,
    trim: true,
    errorMessage: "Please enter a valid password"
  }
})

const registerSchema = checkSchema({
  email: {
    isEmail: true,
    notEmpty: true,
    trim: true,
    normalizeEmail: true,
    errorMessage: "Invalid email address"
  },
  password: {
    notEmpty: true,
    trim: true,
    errorMessage: "Enter a valid password"
  },
  name: {
    notEmpty: true,
    trim: true,
    escape: true,
    isLength: {
      max: 50,
    },
    errorMessage: "Enter a valid name"
  }
})

module.exports = {
  authLimiter,
  loginSchema,
  registerSchema
}