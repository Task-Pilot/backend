const express = require("express")
const cookieParser = require("cookie-parser")

const auth = require("./services/auth")
const api = require("./services/api")

const app = express()

const PORT = process.env.PORT || 3000

app.use(cookieParser())
api.use((req, res, next) => {
	console.log(req.url)
	next()	
})
app.use(auth.session)

//--------------------AUTH---------------------------//

app.use("/auth", auth.route)

//--------------------API ROUTES---------------------//

app.use("/api", api)

//------------------------START-----------------------//

const timestamp = (new Date()).toLocaleString("en-US", { timeZone: "America/Los_Angeles" })

app.listen(PORT, ()=> console.log(`Server started on port ${PORT} at ${timestamp}!`))
