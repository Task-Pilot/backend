const mysql2 = require("mysql2/promise");

const MYSQL_HOST = process.env.MYSQL_HOST || "localhost";
const MYSQL_PASSWORD = process.env.MYSQL_PASSWORD
if (MYSQL_PASSWORD === undefined) {
	throw new Error("MYSQL_PASSWORD is undefined!")
}

const pool = mysql2.createPool({
	connectionLimit: 10,
  host: MYSQL_HOST,
	user: "task-pilot",
  password: MYSQL_PASSWORD,
	database: "task_tracker",
	port: 3306,
	socketPath: "/var/run/mysqld/mysqld.sock"
})

async function createConnection() {
	return await pool.getConnection()
}

module.exports.createConnection = createConnection;